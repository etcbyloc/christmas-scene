import * as THREE from "three";
import { OrbitControls } from "three/addons/controls/OrbitControls.js";
import { GLTFLoader } from "three/addons/loaders/GLTFLoader.js";

//_____________________________________________________________________________
//___________ 1. Chung ________________________________________________________

//   a. Thêm cảnh
const scene = new THREE.Scene();

//   b. Thêm Camera - Perspective Cam cho chiều sâu
const camera = new THREE.PerspectiveCamera(
  75,
  window.innerWidth / window.innerHeight,
  0.1,
  1000
);
camera.position.set(20, 10, -15);
camera.lookAt(0, 2, 0);

//   c. Thêm renderer hiển thị trên web
const renderer = new THREE.WebGLRenderer({ alpha: true, antialias: false });
renderer.toneMapping = THREE.ReinhardToneMapping; //Balanced HDR
renderer.toneMappingExposure = 2.3;
renderer.shadowMap.enabled = true;
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

//   d. Tiện ích: Thêm OrbitControls
const controls = new OrbitControls(camera, renderer.domElement);

//_____________________________________________________________________________
//__________ 2. Các yếu tố trong khung hình ___________________________________

//   a. Ánh sáng
// Point Light - right
const pointLight = new THREE.PointLight(0xffa95c, 50, 20);
pointLight.castShadow = true;
pointLight.position.set(3.8, 4, 1.7);
scene.add(pointLight);
// Point Light 2 - left
const pointLight2 = new THREE.PointLight(0xffa95c, 50, 20);
pointLight2.castShadow = true;
pointLight2.position.set(-4, 4, -4);
scene.add(pointLight2);
// Point Light 3 - front
const pointLight3 = new THREE.PointLight(0xffa95c, 100, 20);
pointLight3.castShadow = true;
pointLight3.position.set(1.8, 4, -3.6);
scene.add(pointLight3);
// Point Light 4 - tree
const pointLight4 = new THREE.PointLight(0xffffff, 50, 5);
pointLight4.castShadow = false;
pointLight4.position.set(3.15, 4, -13.25);
scene.add(pointLight4);
// Hemisphere Light
const hemilight = new THREE.HemisphereLight(0x2e86c1, 0x080820, 0.4);
scene.add(hemilight);
// Spot Light
const spotlight = new THREE.SpotLight(0xffffff, 40, 300, 1000, 1, 1.5);
spotlight.castShadow = true;
spotlight.position.set(0, 20, 0);
scene.add(spotlight);
// Directional Light
const dirLight = new THREE.DirectionalLight(0xaed6f1, 0.5);
dirLight.position.set(150, 100, -10);
dirLight.castShadow = true;
dirLight.shadow.camera.top = 30;
dirLight.shadow.camera.bottom = -20;
dirLight.shadow.camera.left = -20;
dirLight.shadow.camera.right = 20;
dirLight.shadow.camera.near = 1;
dirLight.shadow.camera.far = 300;
scene.add(dirLight);

//   b. Mặt đất
const matdat = new THREE.Mesh(
  new THREE.PlaneGeometry(800, 800),
  new THREE.MeshPhongMaterial({ color: 0xeaecee, depthWrite: true })
);
matdat.rotation.x = -Math.PI / 2;
matdat.receiveShadow = true;
scene.add(matdat);

//   c. Background
scene.background = new THREE.CubeTextureLoader().load([
  "./Elements/Textures/night.jpg",
  "./Elements/Textures/night.jpg",
  "./Elements/Textures/night.jpg",
  "./Elements/Textures/night.jpg",
  "./Elements/Textures/night.jpg",
  "./Elements/Textures/night.jpg",
]);

//   d. Sương mù
scene.fog = new THREE.Fog(0x000000, 1, 500);

//   e. Âm thanh
const listener = new THREE.AudioListener();
camera.add(listener);
const sound = new THREE.Audio(listener);
const audioLoader = new THREE.AudioLoader();
audioLoader.load("Elements/Others/audio.mp3", function (buffer) {
  sound.setBuffer(buffer);
  sound.setLoop(true);
  sound.setVolume(1);
  sound.play();
});

//_____________________________________________________________________________
//___________ 3. Các vật thể chính trong không gian ___________________________

//   a. Mặt trăng
const moongeo = new THREE.SphereGeometry(15, 32, 16);
const moontexture = new THREE.TextureLoader().load(
  "Elements/Textures/moon.jpg"
);
const moonmaterial = new THREE.MeshBasicMaterial({ map: moontexture });
const moon = new THREE.Mesh(moongeo, moonmaterial);
scene.add(moon);
moon.position.set(150, 50, -10);

//   b. Nhà
const house = new GLTFLoader().setPath("Elements/3Dmodels/");
house.load("scene.gltf", (gltf) => {
  const mesh = gltf.scene;
  mesh.position.set(0, 1, 0);
  mesh.scale.set(0.01, 0.01, 0.01);
  mesh.rotation.y = Math.PI / 4;
  mesh.traverse((n) => {
    if (n.isMesh) {
      n.castShadow = true;
      n.receiveShadow = true;
    }
  });
  scene.add(mesh);
});

//   c. Cây thông
const tree = new GLTFLoader().setPath("Elements/3Dmodels/");
tree.load("tree.glb", (gltf) => {
  const mesh = gltf.scene;
  mesh.position.set(3, 0.4, -13.5);
  mesh.scale.set(0.2, 0.2, 0.2);
  mesh.rotation.y = Math.PI / 4;
  mesh.traverse((n) => {
    if (n.isMesh) {
      n.castShadow = true;
      n.receiveShadow = true;
    }
  });
  scene.add(mesh);
});

//   d. Người tuyết 1 - trước cổng
const snowman = new GLTFLoader().setPath("Elements/3Dmodels/");
snowman.load("snowman.glb", (gltf) => {
  const mesh = gltf.scene;
  mesh.position.set(7, 0, -8);
  mesh.scale.set(1, 1, 1);
  mesh.rotation.y = 2;
  mesh.traverse((n) => {
    if (n.isMesh) {
      n.castShadow = true;
      n.receiveShadow = true;
    }
  });
  scene.add(mesh);
});

//   e. Người tuyết 2 - bên phải
const snowman2 = new GLTFLoader().setPath("Elements/3Dmodels/");
snowman2.load("gangsnowman.glb", (gltf) => {
  const mesh = gltf.scene;
  mesh.position.set(-9, 0, -6);
  mesh.scale.set(1.6, 1.6, 1.6);
  mesh.rotation.y = 5;
  mesh.traverse((n) => {
    if (n.isMesh) {
      n.castShadow = true;
      n.receiveShadow = true;
    }
  });
  scene.add(mesh);
});

//   f. Người tuyết múa - trước cổng
const clock = new THREE.Clock();
let mixer;
const snowman3 = new GLTFLoader().setPath("Elements/3Dmodels/");
snowman3.load("snowman8.glb", (gltf) => {
  const mesh = gltf.scene;
  mesh.position.set(16, 0, -2);
  mesh.scale.set(1, 1, 1);
  mesh.rotation.y = -0.3;
  mesh.traverse((n) => {
    if (n.isMesh) {
      n.castShadow = true;
      n.receiveShadow = true;
    }
  });
  scene.add(mesh);
  mixer = new THREE.AnimationMixer(mesh);
  gltf.animations.forEach((clip) => {
    mixer.clipAction(clip).play();
  });
});

//   g. Tuyết
const snowflakeCount = 300; //Số lượng tuyết
const snowflakeGeometry = new THREE.SphereGeometry(0.1, 9, 7);
const snowflakeMaterial = new THREE.MeshBasicMaterial({
  color: 0xffffff,
  transparent: true,
  opacity: 0.6,
});

const snowflakes = new THREE.Group();
for (let i = 0; i < snowflakeCount; i++) {
  const snowflake = new THREE.Mesh(snowflakeGeometry, snowflakeMaterial);
  const x = (Math.random() - 0.5) * 90;
  const y = Math.random() * 35;
  const z = (Math.random() - 0.5) * 90;
  snowflake.position.set(x, y, z);
  snowflakes.add(snowflake);
}
scene.add(snowflakes);

//   h. Khói
const smokeCount = 150;
const smokeGeometry = new THREE.SphereGeometry(0.7, 9, 7);
const smokeMaterial = new THREE.MeshBasicMaterial({
  color: 0xffffff,
  transparent: true,
  opacity: 0.1,
});

const smokes = new THREE.Group();
for (let i = 0; i < smokeCount; i++) {
  const smoke = new THREE.Mesh(smokeGeometry, smokeMaterial);
  const x = Math.random() * 1 + 1;
  const y = Math.random() * 30 + 12;
  const z = Math.random() * 1 + 3;
  //Vì ống khói có tọa độ là (1,12,3)
  smoke.position.set(x, y, z);
  smokes.add(smoke);
}
scene.add(smokes);

//_____________________________________________________________________________
//_________ 4. Hoạt ảnh - Hình động____________________________________________

//   a. Hàm tuyết rơi
function animateSnowflakes() {
  snowflakes.children.forEach((snowflake) => {
    snowflake.position.y -= 0.07;
    if (snowflake.position.y < 0) {
      snowflake.position.y = 40;
    }
  });
}

//    b. Hàm khói bay
function animateSmoke() {
  smokes.children.forEach((smoke) => {
    smoke.position.y += 0.02;
    smoke.scale.x = smoke.position.y * 0.1;
    smoke.scale.y = smoke.position.y * 0.1;
    smoke.scale.z = smoke.position.y * 0.1;
    if (smoke.position.y > 42) {
      smoke.position.y = 12;
      smoke.scale.x = 1;
      smoke.scale.y = 1;
      smoke.scale.z = 1;
    }
  });
}

//   c. Nút chuyển ngày đêm
// Tạo biến ngày đêm
let isDay = false;
const daybackground = new THREE.Color(0xaed6f1);
const nightbackground = new THREE.CubeTextureLoader().load([
  "./Elements/Textures/night.jpg",
  "./Elements/Textures/night.jpg",
  "./Elements/Textures/night.jpg",
  "./Elements/Textures/night.jpg",
  "./Elements/Textures/night.jpg",
  "./Elements/Textures/night.jpg",
]);

// Chuyển hóa ngày đêm
const daylight = new THREE.HemisphereLight(0xaed6f1, 0x080820, 0.6);
function toggleDayNight() {
  isDay = !isDay;
  if (isDay) {
    scene.background = daybackground;
    scene.fog = new THREE.Fog(0xaed6f1, 0.001, 500);
    scene.remove(moon);
    scene.add(daylight);
  } else {
    scene.background = nightbackground;
    scene.fog = new THREE.Fog(0x000000, 0.001, 500);
    scene.add(moon);
    moon.position.set(150, 50, -10);
    scene.remove(daylight);
  }
}
// Nút chuyển hóa ngày đêm
const toggleButton = document.createElement("button");
toggleButton.innerHTML = "Toggle Day/Night";
toggleButton.style.position = "absolute";
toggleButton.style.top = "10px";
toggleButton.style.left = "10px";
document.body.appendChild(toggleButton);
toggleButton.addEventListener("click", toggleDayNight);

//   d. Nút chuyển động Camera
let isFcamEnabled = true;
let clockcam = 20;
let r = 25;
function enablefcam() {
  isFcamEnabled = !isFcamEnabled;
}

function fcam() {
  clockcam += 0.04;
  camera.position.x = Math.sin(clockcam * 0.1) * r;
  camera.position.z = Math.cos(clockcam * 0.1) * r;
  camera.lookAt(0, 2, 0);
}

const fcamToggleButton = document.createElement("button");
fcamToggleButton.innerHTML = "Auto camera";
fcamToggleButton.style.position = "absolute";
fcamToggleButton.style.top = "90px";
fcamToggleButton.style.left = "10px";
document.body.appendChild(fcamToggleButton);
fcamToggleButton.addEventListener("click", enablefcam);

//   e. Nút tắt/bật bóng - chống lag
// Fix bóng không mất
const fixshadow = new THREE.HemisphereLight(0xffffff, 0xffffff, 0);

// Biến tắt/bật shadow
let isShadowEnabled = true;
function enableshadow() {
  isShadowEnabled = !isShadowEnabled;
  if (isShadowEnabled) {
    renderer.shadowMap.enabled = true;
    scene.remove(fixshadow);
  } else {
    renderer.shadowMap.enabled = false;
    scene.add(fixshadow);
  }
}
const shadowToggleButton = document.createElement("button");
shadowToggleButton.innerHTML = "Toggle Shadow";
shadowToggleButton.style.position = "absolute";
shadowToggleButton.style.top = "50px";
shadowToggleButton.style.left = "10px";
document.body.appendChild(shadowToggleButton);
shadowToggleButton.addEventListener("click", enableshadow);

//   f. Nhấp nháy cây thông
let ablink = 0;
function blink() {
  ablink += 1;
  if (ablink % 128 == 0) {
    pointLight4.color.setHex(0xffff00);
  }
  if (ablink % 128 == 32) {
    pointLight4.color.setHex(0x33ff00);
  }
  if (ablink % 128 == 64) {
    pointLight4.color.setHex(0xff0000);
  }
  if (ablink % 128 == 96) {
    pointLight4.color.setHex(0x0000ff);
  }
}

//   g. Animation
function animate() {
  animateSnowflakes();
  animateSmoke();
  requestAnimationFrame(animate);
  if (isFcamEnabled) {
    fcam();
  }
  blink();
  const deltaTime = clock.getDelta();
  if (mixer) {
    mixer.update(deltaTime);
  }

  renderer.render(scene, camera);
}

animate();
